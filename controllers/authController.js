const { User } = require('../models')

jwtResponsesFormatter = (user) => {
    let format = {
        id : user.id,
        username : user.username,
        accessToken : user.generateToken()
    }

    return format
}

module.exports = {
    createUser : async (req,res) => {
        try {
            let userCreated = await User.register(req.body.username, req.body.password)

            res.redirect('/login')
        }catch(err){
            console.log(err)
            res.json(500, "Error")
        }
    },

    authUser : async (req,res) => {
        try{
            let checkUser = await User.authentication(req.body.username, req.body.password)
            return res.json(jwtResponsesFormatter(checkUser))
            
        }catch(err){
            return res.json(500, err)
        }
    }
}