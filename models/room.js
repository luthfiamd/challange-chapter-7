'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Room.belongsTo(models.UserGames, {
        foreignKey: 'player1',
        targetKey: 'id'
      })
      Room.belongsTo(models.UserGames, {
        foreignKey: 'player2',
        targetKey: 'id'
      })
    }
  };
  Room.init({
    player1: DataTypes.INTEGER,
    player2: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};
