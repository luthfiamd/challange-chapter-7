const express = require('express')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
const { PORT = 8000 } = process.env

const authRouter = require('./routers/authRouter')

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))

const passport = require('./lib/passport')
app.use(passport.initialize());

app.use(authRouter)


app.listen(PORT, () => {
    console.log(`Server started on port `, PORT);
});
